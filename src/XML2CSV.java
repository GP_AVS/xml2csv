import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

class XML2CSV {
	private static String FLATTEN_XSL_FILENAME = "src/flatten.xsl";
	private static String COLUMNISE_XSL_FILENAME = "src/columnise.xsl";
	private static String TEMP_FILENAME = "tmp/flat.xml";

	
    public static void main(String args[]) throws Exception {
    	if(args.length == 2) {
    		String sourcePath = args[0];
    		String targetPath = args[1];
    	
	        File flatXSLT = new File(FLATTEN_XSL_FILENAME);
	        File colXSLT = new File(COLUMNISE_XSL_FILENAME);
	        File xmlSource = new File(sourcePath);
	        
	        System.out.println("Parsing input file");
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        Document document = builder.parse(xmlSource);
	
	        System.out.println("Loading flatten XSLT");
	        StreamSource stylesource = new StreamSource(flatXSLT);
	        Transformer transformer = TransformerFactory.newInstance()
	                .newTransformer(stylesource);
	        
	        System.out.println("Writing temp flattened XML");
	        Source source = new DOMSource(document);
	        Result outputTarget = new StreamResult(new File(TEMP_FILENAME));
	        transformer.transform(source, outputTarget);
	        
	        System.out.println("Parsing flattened XML");
	        File tmpSource = new File(TEMP_FILENAME);
	        document = builder.parse(tmpSource);
	        
	        System.out.println("Loading columnise XSLT");
	        stylesource = new StreamSource(colXSLT);
	        transformer = TransformerFactory.newInstance()
	                .newTransformer(stylesource);
	        
	        System.out.println("Writing CSV");
	        source = new DOMSource(document);
	        outputTarget = new StreamResult(new File(targetPath));
	        transformer.transform(source, outputTarget);
	        
	        System.out.println("Clean up");
	        tmpSource.delete();
	        
    	} else {
    		System.out.println("USAGE: ApplyXSLT [source] [target]");
    	}
    	
    }
}