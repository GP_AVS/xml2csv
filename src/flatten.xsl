<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="AE539">
		<AE539_Flat>
			<fldIsSigned><xsl:value-of select="fldIsSigned"/></fldIsSigned>
			<fldReturnOriginatorStatus><xsl:value-of select="fldReturnOriginatorStatus"/></fldReturnOriginatorStatus>
			<fldClassification><xsl:value-of select="fldClassification"/></fldClassification>
			<fldFormNumber><xsl:value-of select="fldFormNumber"/></fldFormNumber>
			<fldIdent><xsl:value-of select="fldIdent"/></fldIdent>
			<xsl:apply-templates select="subPreliminaries"/> 
			<xsl:apply-templates select="subBackgroundandContext"/> 
			<xsl:apply-templates select="subSpecificRequirements"/>
		</AE539_Flat>
	</xsl:template>

	<xsl:template match="subPreliminaries">
		<fldObjectiveObjectID><xsl:value-of select="subObjectiveID/fldObjectiveObjectID"/></fldObjectiveObjectID>
		<ddlAMLE><xsl:value-of select="subAMLE_ARDRNumber/subAMLE/ddlAMLE"/></ddlAMLE>
		<ddlARDRTitle><xsl:value-of select="subAMLE_ARDRNumber/subAMLE/ddlARDRTitle"/></ddlARDRTitle>
		<dteCorrectAsAt><xsl:value-of select="subAMLE_ARDRNumber/subCorrectAsAt/dteCorrectAsAt"/></dteCorrectAsAt>
		<fldARDRNumber><xsl:value-of select="subAMLE_ARDRNumber/subARDRNumber/fldARDRNumber"/></fldARDRNumber>
		<fldARDRDescription><xsl:value-of select="subARDRDescription/fldARDRDescription"/></fldARDRDescription>
		<ddlAMLE_RankTitle><xsl:value-of select="subAMLELeadDetails/ddlAMLE_RankTitle"/></ddlAMLE_RankTitle>
		<fldAMLE_Initials><xsl:value-of select="subAMLELeadDetails/fldAMLE_Initials"/></fldAMLE_Initials>
		<fldAMLE_Surname><xsl:value-of select="subAMLELeadDetails/fldAMLE_Surname"/></fldAMLE_Surname>
		<fldAMLE_PositionAppointment><xsl:value-of select="subAMLELeadDetails/fldAMLE_PositionAppointment"/></fldAMLE_PositionAppointment>
		<fldAMLE_Unit><xsl:value-of select="subAMLELeadDetails/fldAMLE_Unit"/></fldAMLE_Unit>
		<fldAMLE_PhoneNo><xsl:value-of select="subAMLELeadDetails/fldAMLE_PhoneNo"/></fldAMLE_PhoneNo>
		<fldAMLE_FaxNo><xsl:value-of select="subAMLELeadDetails/fldAMLE_FaxNo"/></fldAMLE_FaxNo>
		<fldAMLE_MobileNo><xsl:value-of select="subAMLELeadDetails/fldAMLE_MobileNo"/></fldAMLE_MobileNo>
		<fldAMLE_EmailAddress><xsl:value-of select="subAMLELeadDetails/subAMLE_EmailAddress/fldAMLE_EmailAddress"/></fldAMLE_EmailAddress>
		<grpSAAOfficeYesNo><xsl:value-of select="subApproachARDRNumber/grpSAAOfficeYesNo"/></grpSAAOfficeYesNo>
	</xsl:template>

	<xsl:template match="subBackgroundandContext">
		<fldBackground><xsl:value-of select="fldBackground"/></fldBackground>
		<fldContext><xsl:value-of select="fldContext"/></fldContext>
	</xsl:template>

	<xsl:template match="subSpecificRequirements">

		<xsl:for-each select="subRequirements/subRequirement">
		<subRequirement>
			<fldRequirementTitle><xsl:value-of select="fldRequirementTitle"/></fldRequirementTitle>
			<ddlRequirementPriority><xsl:value-of select="ddlRequirementPriority"/></ddlRequirementPriority>
			<ddlRequirementPrecedence><xsl:value-of select="ddlRequirementPrecedence"/></ddlRequirementPrecedence>
			<ddlRequirementPlanningHorizon><xsl:value-of select="ddlRequirementPlanningHorizon"/></ddlRequirementPlanningHorizon>
			<fldRequirementOwner_PositionAppointment><xsl:value-of select="fldRequirementOwner_PositionAppointment"/></fldRequirementOwner_PositionAppointment>
			<fldRequirementOwner_EmailAddress><xsl:value-of select="fldRequirementOwner_EmailAddress"/></fldRequirementOwner_EmailAddress>
			<fldRequirementDescription><xsl:value-of select="fldRequirementDescription"/></fldRequirementDescription>
			<fldRequirementExpectedOutcomes><xsl:value-of select="fldRequirementExpectedOutcomes"/></fldRequirementExpectedOutcomes>
			<ddlRequirementDeskOfficer_RankTitle><xsl:value-of select="subRequirementDeskOfficer/ddlRequirementDeskOfficer_RankTitle"/></ddlRequirementDeskOfficer_RankTitle>
			<fldRequirementDeskOfficer_Initials><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_Initials"/></fldRequirementDeskOfficer_Initials>
			<fldRequirementDeskOfficer_Surname><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_Surname"/></fldRequirementDeskOfficer_Surname>
			<fldRequirementDeskOfficer_Appointment><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_Appointment"/></fldRequirementDeskOfficer_Appointment>
			<fldRequirementDeskOfficer_Unit><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_Unit"/></fldRequirementDeskOfficer_Unit>
			<fldRequirementDeskOfficer_PhoneNo><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_PhoneNo"/></fldRequirementDeskOfficer_PhoneNo>
			<fldRequirementDeskOfficer_FaxNo><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_FaxNo"/></fldRequirementDeskOfficer_FaxNo>
			<fldRequirementDeskOfficer_MobileNo><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_MobileNo"/></fldRequirementDeskOfficer_MobileNo>
			<fldRequirementDeskOfficer_EmailAddress><xsl:value-of select="subRequirementDeskOfficer/fldRequirementDeskOfficer_EmailAddress"/></fldRequirementDeskOfficer_EmailAddress>
			<xsl:apply-templates select="subDeliverables"/>
		</subRequirement>
		</xsl:for-each>

	</xsl:template>
	
	<xsl:template match="subDeliverables">
		
		<xsl:for-each select="subDeliverable">
		<subDeliverable>
			<fldDeliverableSerial><xsl:value-of select="fldDeliverableSerial"/></fldDeliverableSerial>
			<ddlDeliverablePriority><xsl:value-of select="ddlDeliverablePriority"/></ddlDeliverablePriority>
			<dteDeliverableDueDate><xsl:value-of select="dteDeliverableDueDate"/></dteDeliverableDueDate>
			<fldDeliverableDescription><xsl:value-of select="fldDeliverableDescription"/></fldDeliverableDescription>
			<ddlDeliverableSANDA1><xsl:value-of select="ddlDeliverableSANDA1"/></ddlDeliverableSANDA1>
			<ddlDeliverableSANDA2><xsl:value-of select="ddlDeliverableSANDA2"/></ddlDeliverableSANDA2>
			<ddlDeliverableSANDA3><xsl:value-of select="ddlDeliverableSANDA3"/></ddlDeliverableSANDA3>
			<fldDeliverableSANDA1Other><xsl:value-of select="fldDeliverableSANDA1Other"/></fldDeliverableSANDA1Other>
			<fldDeliverableSANDA2Other><xsl:value-of select="fldDeliverableSANDA2Other"/></fldDeliverableSANDA2Other>
			<fldDeliverableSANDA3Other><xsl:value-of select="fldDeliverableSANDA3Other"/></fldDeliverableSANDA3Other>
		</subDeliverable>
		</xsl:for-each>
		
	</xsl:template>
</xsl:stylesheet>